package appname.view;


import appname.model.ModelName;
import javafx.stage.Window;

public class AppNamePresenter {
  final private ModelName model;
  final private AppNameView view;

  public AppNamePresenter(ModelName model, AppNameView view) {
    this.model = model;
    this.view = view;
    addEventHandlers();
    updateView();
  }

  private void addEventHandlers() {
    // TODO
    //   add event handlers
    //   that change the underlying data
    //   first update the model
    //   and then call updateView()

    // TODO
    //    add window event handlers
    addWindowEventHandlers();
  }

  private void updateView() {
    // TODO fill the view with model data
  }

  public void addWindowEventHandlers() {
    Window window = view.getScene().getWindow();
    //TODO  add  event handlers to window
  }
}
